package com.broscorp.repository;

import com.broscorp.model.Tour;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository that manages {@link Tour} entity
 */
public interface TourRepository extends JpaRepository<Tour, Long> {

    List<Tour> findByNameContainingIgnoreCase(String nameFilter);
}
