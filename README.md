# SpringSampleProject

HOW TO RUN 
1. navigate to root of the project
2. mvn clean package
3. cd target
4. java -jar backend-1.0-SNAPSHOT.jar

HOT TO RUN TESTS
1. navigate to root of the project
2. mvn clean test

HOT TO MEASURE CODE COVERAGE
1. navigate to root of the project
2. mvn clean test jacoco:report
3. navigate to target/jacoco-ut
4. open index.html in browser

Specification for the project:  
Goal:  
Create a simple web application that  
● allows populating its database with sightseeing tours from a remote JSON endpoint  
● allows listing the tours from its database  

User Stories:  
As an Admin, I want to instruct the application to refresh its inventory from a remote
JSON endpoint so that the application has a new inventory of tours  

Acceptance criteria:  
● There is a REST endpoint to service the refresh request
○ Path: /tours/refresh
○ Method: POST
○ POST body (application/json) :
{ ‘filter’: string | null }
e.g.
{ ‘filter’: ‘history’}
○ Response
■ HTTP 200: in case the tour import (refresh) was successful
■ HTTP 401: when the endpoint is called without or with wrong
authentication credentials
■ HTTP 403: when the user does not have the Admin role
■ HTTP 500: in case the inventory cannot be retrieved from the remote
JSON endpoint
● When the filter is null then all tours should be imported from the remote JSON file.
● When the filter is a given word then only tours whose name contain the given word
(using case insensitive search) are imported.
● We do not need to support any other filtering criteria
● The endpoint is protected with HTTP basic authentication.
● The remote JSON endpoint to import the tours from is:
https://s3-eu-west-1.amazonaws.com/pocketguide/_test/store_en.v2.gz  

As a User, I want to list tours so that I can see the tours in the application’s inventory

Acceptance criteria:  
● There is a REST endpoint to service the tour listing request
○ Path: /tours
○ Method: GET
○ Query parameters:
■ filter: [optional], if given only tours whose name contains the given
word (using case insensitive search) are enlisted. If filter is not given,
then all tours are enlisted.
○ Response
■ HTTP 200: in case of success
● Response body (application/json): JSON array with the
enlisted tour names. E.g.
[‘Paris Sightseeing Tours’, ‘London Soho’,
‘Ancient Rome’’]
Empty array is returned when the request results in no tours.
■ HTTP 401: when the endpoint is called without or with wrong
authentication credentials
■ HTTP 403: when the user does not have the User role
● The endpoint is protected with HTTP basic authentication.
The tour inventory should be persisted.  

Acceptance criteria:  
● Persist the tours in an SQL database.
The structure of this JSON document is:
{
cities: array (you can ignore this part)
bundles: array (you can ignore this part)
tours: array
[
{
id: number,
name: string,
longDesc: string,
…
},
…
]
}  

It is enough to persist the following data for a tour from the remote JSON file:  
○ name  
○ longDesc  
● For this exercise, an in-memory database (e.g. HSQLDB) will suffice. However, it should
be only a matter of configuration to switch to e.g. MySQL.  
● For the sake of this exercise, please use Hibernate/JPA as an ORM framework.  
Predefine 2 users, one in the Admin role and one in the User role
Acceptance criteria:
● Predefine a user in the Admin role:  
○ Username: admin
○ Password: admin12
● Predefine a user in the User role:  
○ Username: john
○ Password: john12
● Let these predefined users be persisted in the database.