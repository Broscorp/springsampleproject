package com.broscorp.service;

import com.broscorp.model.Tour;
import com.broscorp.repository.TourRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TourServiceTest {

    @Mock
    private TourRepository tourRepository;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private TourService tourService;

    @Test
    public void getToursWithNoFilter() {
        //setup
        String filter = null;
        Tour actualTour = new Tour();
        actualTour.setName("test entry");
        when(tourRepository.findAll()).thenReturn(List.of(actualTour));
        //execute
        List<String> tourList = tourService.getTours(filter);
        //verify
        assertNotNull(tourList);
        assertEquals(1, tourList.size());
        assertEquals("test entry", tourList.get(0));
    }


    @Test
    public void getToursWithFilter() {
        //setup
        String filter = "test";
        Tour actualTour = new Tour();
        actualTour.setName("test entry");
        when(tourRepository.findByNameContainingIgnoreCase(filter)).thenReturn(List.of(actualTour));
        //execute
        List<String> tourList = tourService.getTours(filter);
        //verify
        assertNotNull(tourList);
        assertEquals(1, tourList.size());
        assertEquals("test entry", tourList.get(0));
    }


    @Test
    public void getToursEmptyResult() {
        //setup
        String filter = null;
        when(tourRepository.findAll()).thenReturn(Collections.emptyList());
        //execute
        List<String> tourList = tourService.getTours(filter);
        //verify
        assertNotNull(tourList);
        assertEquals(0, tourList.size());
    }

    @Test
    public void refreshToursNotContains() {
        //setup
        String filter = null;
        when(restTemplate
                .exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenThrow(RuntimeException.class);
        //execute & verify
        assertThrows(RuntimeException.class, () -> tourService.refreshTours(filter));
    }


    @Test
    public void refreshToursRemoteJsonCantFetch() {
        //setup
        String filter = null;
        when(restTemplate
                .exchange(anyString(), any(HttpMethod.class), any(HttpEntity.class), any(Class.class)))
                .thenThrow(RuntimeException.class);
        //execute & verify
        assertThrows(RuntimeException.class, () -> tourService.refreshTours(filter));
    }
}