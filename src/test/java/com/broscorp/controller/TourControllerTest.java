package com.broscorp.controller;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.broscorp.exception.JsonFetchFailedException;
import com.broscorp.service.TourService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TourControllerTest {


    @Autowired
    private TourController tourController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TourService tourService;

    @Test
    public void contexLoads() {
        assertThat(tourController).isNotNull();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    public void testRefreshToursOk() throws Exception {
        //execute and verify
        mockMvc.perform(post("/tours/refresh")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"filter\":null}"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    public void testRefreshTours500() throws Exception {
        //setup
        doThrow(new JsonFetchFailedException()).when(tourService).refreshTours(null);
        //execute & verify
        mockMvc.perform(post("/tours/refresh")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"filter\":null}"))
                .andExpect(status().is5xxServerError())
                .andReturn();
    }

    @WithMockUser(username = "not_an_admin", roles = "USER")
    @Test
    public void testRefreshTours403() throws Exception {
        //execute & verify
        mockMvc.perform(post("/tours/refresh")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"filter\":null}"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testRefreshTours401() throws Exception {
        //execute & verify
        mockMvc.perform(post("/tours/refresh")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{\"filter\":null}"))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }


    @WithMockUser(username = "user", roles = "USER")
    @Test
    public void testFetchToursOk() throws Exception {
        //execute and verify
        mockMvc.perform(get("/tours")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
    }


    @WithMockUser(username = "admin", roles = "ADMIN")
    @Test
    public void testFetchTours403() throws Exception {
        //execute & verify
        mockMvc.perform(get("/tours")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void testFetchTours401() throws Exception {
        //execute & verify
        mockMvc.perform(get("/tours")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized())
                .andReturn();
    }
}
