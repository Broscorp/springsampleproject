package com.broscorp.repository;

import com.broscorp.model.Tour;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TourRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TourRepository tourRepository;

    @Test
    public void testFindByNameContains() {
        //setup
        Tour expectedTour = new Tour();
        expectedTour.setName("testTour");
        expectedTour.setId(1L);
        expectedTour.setLongDesc("LongDesc");
        tourRepository.save(expectedTour);
        //execute
        List<Tour> tourList = tourRepository.findByNameContainingIgnoreCase("test");
        //verify
        assertNotNull(tourList);
        assertEquals(1, tourList.size());
        assertEquals(expectedTour.getName(), tourList.get(0).getName());
    }

    @Test
    public void testFindByNameNotContains() {
        //setup
        Tour expectedTour = new Tour();
        expectedTour.setName("testTour");
        expectedTour.setId(1L);
        expectedTour.setLongDesc("LongDesc");
        tourRepository.save(expectedTour);
        //execute
        List<Tour> tourList = tourRepository.findByNameContainingIgnoreCase("something");
        //verify
        assertNotNull(tourList);
        assertEquals(0, tourList.size());
    }


}
