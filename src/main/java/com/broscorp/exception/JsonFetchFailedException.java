package com.broscorp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception that indicates that fetching remote json has failed.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class JsonFetchFailedException extends RuntimeException {

    public JsonFetchFailedException() {
        super("Couldn't retrieve remote tours json");
    }
}
