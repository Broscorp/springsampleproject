package com.broscorp.controller;

import com.broscorp.service.TourService;
import java.util.List;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/tours")
@Slf4j
public class TourController {


    private final TourService tourService;

    public TourController(TourService tourService) {
        this.tourService = tourService;
    }

    @PostMapping(path = "/refresh", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    public void refreshTours(@RequestBody(required = false) FilterObject filterObj) {
        log.info("Accessing /refresh endpoint with filter = {}", filterObj.filter != null ?
            filterObj.filter : "empty");
        tourService.refreshTours(filterObj.filter);
    }


    @GetMapping
    @PreAuthorize("hasRole('USER')")
    public List<String> tours(@RequestParam(value = "filter", required = false) String filter) {
        log.info("Accessing /tours endpoint with filter = {}", filter);
        return tourService.getTours(filter);
    }

    /**
     * Special filter object that encapsulates filter value.
     */
    @Data
    private static class FilterObject {

        public String filter;
    }
}
