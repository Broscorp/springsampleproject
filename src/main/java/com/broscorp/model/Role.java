package com.broscorp.model;

public enum Role {
    ADMIN,
    USER
}
