package com.broscorp.service;

import com.broscorp.exception.JsonFetchFailedException;
import com.broscorp.model.Tour;
import com.broscorp.repository.TourRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * Service that performs business logic related to {@link Tour} entity
 */
@Service
@Slf4j
public class TourService {

    @Value("${remote.json.endpoint}")
    private String jsonUrl;


    private final TourRepository tourRepository;
    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public TourService(TourRepository tourRepository,
                       RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.tourRepository = tourRepository;
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    /**
     * Cleans up the whole db and then fetches new list
     */
    @Transactional
    public void refreshTours(String filterValue) {
        tourRepository.deleteAll();
        List<Tour> newTours = fetchTours(filterValue);
        tourRepository.saveAll(newTours);
        log.info("Refresh completed. {} of new tours were saved.", newTours.size());
    }

    /**
     * Fetchs tours from db.
     *
     * @return list of Tour names
     */
    public List<String> getTours(String name) {
        List<Tour> tours = new ArrayList<>();
        if (StringUtils.isEmpty(name)) {
            tours.addAll(tourRepository.findAll());
        } else {
            tours.addAll(tourRepository.findByNameContainingIgnoreCase(name));
        }
        return tours.stream().map(Tour::getName).collect(Collectors.toList());
    }


    private List<Tour> fetchTours(String filterValue) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCEPT_ENCODING, "application/gzip");
            HttpEntity entity = new HttpEntity(headers);

            ResponseEntity<String> response
                    = restTemplate.exchange(jsonUrl, HttpMethod.GET, entity, String.class);
            JsonNode root = objectMapper.readTree(response.getBody());
            JsonNode toursArray = root.path("tours");
            List<Tour> tours = new ArrayList<>();
            for (JsonNode singleTour : toursArray) {
                Tour tour = objectMapper.treeToValue(singleTour, Tour.class);
                if (StringUtils.isEmpty(filterValue) ||
                    StringUtils.containsIgnoreCase(tour.getName(), filterValue)) {
                    tours.add(tour);
                }
            }
            return tours;
        } catch (IOException ex) {
            log.error("Exception happened while retrieving json", ex);
            throw new JsonFetchFailedException();
        }
    }

}
