package com.broscorp.repository;

import com.broscorp.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository that manages {@link User} entity
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
